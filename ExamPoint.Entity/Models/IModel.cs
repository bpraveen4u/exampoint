﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamPoint.Entity.Models
{
    public interface IModel : IEntity
    {
        int Id { get; set; }
        int Sort { get; set; }
        DateTime? CreatedDate { get; set; }
        string CreatedUser { get; set; }
        DateTime? ModifiedDate { get; set; }
        string ModifiedUser { get; set; }
    }

    public interface IEntity
    {
        int Id { get; set; }
    }
}
