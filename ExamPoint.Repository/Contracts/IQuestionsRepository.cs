﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExamPoint.Entity.Models;
using ExamPoint.Repository.Infrastructure;

namespace ExamPoint.Repository.Contracts
{
    public interface IQuestionsRepository : IRepository<Question>
    {
        void SomeMethod();
    }
}
