﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExamPoint.Repository.Infrastructure;
using ExamPoint.Repository.Contracts;

namespace ExamPoint.Repository
{
    public interface IDALContext : IUnitOfWork
    {
        IQuestionsRepository Questions { get; }
        //IAssessmentRepository Assessments { get; }
        //IUserRepository Users { get; }
        //IUserAnswerRepository UserAnswers { get; }
    }

    public class DALContext : IDALContext
    {
        private DB dbContext;
        private IQuestionsRepository questions;
        //private IAssessmentRepository assessments;
        //private IUserRepository users;
        //private IUserAnswerRepository userAnswers;

        public DALContext()
        {
            dbContext = new DB();
        }

        public IQuestionsRepository Questions
        {
            get
            {
                if (questions == null)
                    questions = new QuestionsRepository(dbContext);
                return questions;
            }
        }

        //public IAssessmentRepository Assessments
        //{
        //    get
        //    {
        //        if (assessments == null)
        //            assessments = new AssessmentRepository(dbContext);
        //        return assessments;
        //    }
        //}

        //public IUserRepository Users
        //{
        //    get
        //    {
        //        if (users == null)
        //            users = new UserRepository(dbContext);
        //        return users;
        //    }
        //}

        //public IUserAnswerRepository UserAnswers
        //{
        //    get
        //    {
        //        if (userAnswers == null)
        //            userAnswers = new UserAnswerRepository(dbContext);
        //        return userAnswers;
        //    }
        //}

        public int SaveChanges()
        {
            return dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (questions != null)
                questions.Dispose();
            //if (assessments != null)
            //    assessments.Dispose();
            if (dbContext != null)
                dbContext.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
