﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using ExamPoint.Repository.Mapping;

namespace ExamPoint.Repository.Infrastructure
{
    public class DB : DbContext, IDbContext
    {
        public DB()
            : base("exampoin_DbExamPoint")
        {

        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !String.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
            type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }*/

            modelBuilder.Configurations.Add(new QuestionMap());
            modelBuilder.Configurations.Add(new OptionMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
