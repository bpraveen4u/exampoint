﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamPoint.Repository.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
    }
}
