﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.ModelConfiguration;
using ExamPoint.Entity.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamPoint.Repository.Mapping
{
    public class OptionMap : EntityTypeConfiguration<Option>
    {
        public OptionMap()
        {
            ToTable("TblOptions");
            HasKey(q => q.Id).Property(q => q.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(q => q.Text).IsRequired().HasMaxLength(1000);
        }
    }
}
