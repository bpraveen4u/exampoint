﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.ModelConfiguration;
using ExamPoint.Entity.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamPoint.Repository.Mapping
{
    public class QuestionMap : EntityTypeConfiguration<Question>
    {
        public QuestionMap()
        {
            ToTable("TblQuestions");
            HasKey(q => q.Id).Property(q => q.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(q => q.Description).IsRequired().HasMaxLength(1000);
            Property(q => q.Category).HasMaxLength(100);
            Property(q => q.InstructorRemarks).HasMaxLength(1000);
            Property(q => q.CreatedUser).IsRequired().HasMaxLength(100);
            Property(q => q.ModifiedUser).IsRequired().HasMaxLength(100);
        }
    }
}
