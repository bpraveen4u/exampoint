﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExamPoint.Repository.Contracts;
using ExamPoint.Repository.Infrastructure;
using ExamPoint.Entity.Models;

namespace ExamPoint.Repository
{
    public class QuestionsRepository : Repository<Question>, IQuestionsRepository
    {
        public QuestionsRepository(DB context)
            : base(context)
        {
        }

        public void SomeMethod()
        {
            throw new NotImplementedException();
        }
    }
}
